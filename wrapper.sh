#!/bin/sh
echo "Running. Press Ctrl+C to stop."
while true; do
    python heroes_last.py
    git add --all
    git commit -m'Auto commit done by wrapper.sh'
    git push -u origin master
    sleep 6s
done
